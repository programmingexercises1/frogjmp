def solution(X, A):
    leaves = set(range(1, X+1))
    for i, n in enumerate(A):
        leaves.discard(n)
        if len(leaves) == 0:
            return i

    return -1   
